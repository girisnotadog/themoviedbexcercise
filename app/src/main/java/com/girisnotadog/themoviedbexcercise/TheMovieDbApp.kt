package com.girisnotadog.themoviedbexcercise

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.girisnotadog.themoviedbexcercise.database.LocalStorage

class TheMovieDbApp : Application() {
    lateinit var localStorage : LocalStorage

    companion object {
        private lateinit var INSTANCE : TheMovieDbApp

        @JvmStatic
        fun getInstance() : TheMovieDbApp {
            if(INSTANCE == null)
                INSTANCE = TheMovieDbApp()

            return INSTANCE
        }
    }

    override fun onCreate() {
        super.onCreate()

        // Deshabilitamos el modo oscuro
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        INSTANCE = this
        localStorage = LocalStorage(this)
        localStorage.storage.registerOnSharedPreferenceChangeListener(localStorage)
    }
}