package com.girisnotadog.themoviedbexcercise.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.girisnotadog.themoviedbexcercise.TheMovieDbApp
import com.girisnotadog.themoviedbexcercise.api.Resource
import com.girisnotadog.themoviedbexcercise.repositories.SettingsRepository
import kotlinx.coroutines.launch

class SettingsViewModel(application: Application) : AndroidViewModel(application) {
    private val lstorage = TheMovieDbApp.getInstance().localStorage
    private val settingsRepository = SettingsRepository()

    val api_key = MutableLiveData<String>("")
    val user_session_id = MutableLiveData<String>("")
    val account_id = MutableLiveData<Int>(0)

    init {
        api_key.value = lstorage.getApiKey()
        user_session_id.value = lstorage.getUserSessionId()
        account_id.value = lstorage.getAccountId()
    }

    fun getUserSessionIdLS() = lstorage.getUserSessionId()
    fun createRequestToken() = settingsRepository.createRequestToken()

    //fun refreshSessionId() = settingsRepository.getSessionId()
    fun refreshSessionId() = settingsRepository.getGuestSessionId()

    fun getAccountId() = settingsRepository.getAccountId()


    fun saveSettings() {
        api_key.value?.let {
            lstorage.setApiKey(it)
        }

        user_session_id.value?.let {
            lstorage.setUserSessionId(it)
        }
    }
}