package com.girisnotadog.themoviedbexcercise.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.girisnotadog.themoviedbexcercise.database.AppDatabase
import com.girisnotadog.themoviedbexcercise.repositories.MoviesRepository

class MediaListViewModel(application : Application) : AndroidViewModel(application) {
    private val database = AppDatabase.getInstance(application.applicationContext)

    fun getMostPopularMovies() = MoviesRepository(database).getMostPopularMovies()

    fun getNowPlayingMovies() = MoviesRepository(database).getNowPlayingMovies()
}