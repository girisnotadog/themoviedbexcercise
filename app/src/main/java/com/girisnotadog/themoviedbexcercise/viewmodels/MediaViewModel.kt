package com.girisnotadog.themoviedbexcercise.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.girisnotadog.themoviedbexcercise.database.AppDatabase
import com.girisnotadog.themoviedbexcercise.database.models.domain.MediaItem
import com.girisnotadog.themoviedbexcercise.repositories.MoviesRepository

class MediaViewModel(application: Application) : AndroidViewModel(application) {
    private val database = AppDatabase.getInstance(application.applicationContext)
    private val moviesRepository = MoviesRepository(database)

    val media : MutableLiveData<MediaItem>
    val tab : MutableLiveData<Int>

    init {
        media = MutableLiveData(MediaItem())
        tab = MutableLiveData(2)
    }

    fun setFavorite(mediaItem: MediaItem) = moviesRepository.setFavorite(mediaItem.id, ! mediaItem.favorite)
    fun refreshFavoriteList() = moviesRepository.refreshFavorites()
    fun mediaVideos(mediaItem: MediaItem) = moviesRepository.getMediaVideos(mediaItem.id)
}