package com.girisnotadog.themoviedbexcercise.database.models.domain

data class MovieEntity (
    var videos : Boolean,
    var playingNow : Boolean,
    var mostRated : Boolean,
    var releaseDate : String
) : MediaItem()
