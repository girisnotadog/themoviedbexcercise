package com.girisnotadog.themoviedbexcercise.database

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import java.lang.Exception

class LocalStorage(context: Context) : SharedPreferences.OnSharedPreferenceChangeListener {
    private val sharedPreferencesName = "THE_MOVIE_DB_LOCAL_STORAGE"

    private val JWT_TOKEN_KEY = "JWT_TOKEN_KEY"
    private val API_KEY = "API_KEY"
    private val USER_SESSION_ID_KEY = "USER_SESSION_ID_KEY"
    private val USER_ACCOUNT_ID_KEY = "USER_ACCOUNT_ID_KEY"
    private val REQUEST_TOKEN_KEY = "REQUEST_TOKEN_KEY"

    val storage = context.getSharedPreferences(sharedPreferencesName, Context.MODE_PRIVATE)

//    @Synchronized
//    fun setJwtToken(token : String) {
//        Log.d("setJwtToken", "Estableciendo token: ${token}")
//        if(token.isNotBlank())
//            storage.edit().putString(JWT_TOKEN_KEY, token).apply()
//        else
//            Log.d("setJwtToken", "Intento de vaciar el token")
//    }
//
//    fun getJwtToken() : String {
//        var jwt = ""
//        try {
//            jwt = storage.getString(JWT_TOKEN_KEY, "") !!
//
//        }catch (e : Exception) {
//            Log.d("e", e.localizedMessage)
//        }
//        Log.d("JWT", jwt)
//        return "{$jwt}"
//    }
//    fun clearJwtToken() = storage.edit().putString(JWT_TOKEN_KEY, "").apply()

    fun getApiKey() : String = "${storage.getString(API_KEY, "")}"
    fun setApiKey(api_key : String) = storage.edit().putString(API_KEY, api_key).apply()

    fun getUserSessionId() : String? = storage.getString(USER_SESSION_ID_KEY, "")
    fun setUserSessionId(session_id : String) = storage.edit().putString(USER_SESSION_ID_KEY, session_id).apply()

    fun getAccountId() : Int = storage.getInt(USER_ACCOUNT_ID_KEY, 0)
    fun setUserAccountId(account_id : Int)  = storage.edit().putInt("${account_id}", 0).apply()

    fun getRequestToken() : String = "${storage.getString(REQUEST_TOKEN_KEY, "")}"
    fun setRequestToken(request_token : String) = storage.edit().putString(REQUEST_TOKEN_KEY, request_token).commit()

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        key?.let {
            Log.d("SharedPrefsChanged", "${key}")
        }
    }
}