package com.girisnotadog.themoviedbexcercise.database.models.domain

open class MediaItem(
    open var id : Int = 0,
    open var title : String = "",
    open var launch_date : String = "",
    open var type : MediaType = MediaType.UNDEFINED,
    open var genres : String = "",
    open var adult : Boolean = false,
    open var favorite : Boolean = false,
    open var image : String = "",
)

enum class MediaType {
    UNDEFINED,
    MOVIE,
    TV_SHOW
}