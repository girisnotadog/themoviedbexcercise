package com.girisnotadog.themoviedbexcercise.database.models.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: T) : Long

    @Insert
    fun insert(vararg obj: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(list : List<T>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAll(list : List<T>)

    @Delete
    fun delete(obj: T)

    @Update
    fun update(obj: T) : Int
}