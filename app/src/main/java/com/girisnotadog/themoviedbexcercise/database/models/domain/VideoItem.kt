package com.girisnotadog.themoviedbexcercise.database.models.domain

data class VideoItem(
    var id : Int,
    var name : String,
    var key : String,
    var official : Boolean,
    var site : String
)
