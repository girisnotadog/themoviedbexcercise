package com.girisnotadog.themoviedbexcercise.database

import androidx.room.TypeConverter
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun arrayIntToString(intList : ArrayList<Int>?) : String? {
        intList?.let {
            return it.joinToString(";")
        }

        return null
    }

    @TypeConverter
    fun stringToArrayInt(stringList: String?) : ArrayList<Int> {
        stringList?.let {
            if(it.isNotEmpty())
                return it.split(";").mapTo(ArrayList<Int>()) {
                    it.toInt()
                }
        }
        return arrayListOf<Int>()
    }

    @TypeConverter
    fun stringToArrayString(string: String?) : ArrayList<String> {
        string?.let {
            if(it.isNotEmpty())
                return it.split(",").mapTo(ArrayList<String>(), {
                    it
                })
        }
        return arrayListOf<String>()
    }

    @TypeConverter
    fun arrayStringToString(array : ArrayList<String>?) : String? {
        array?.let {
            return it.joinToString(",")
        }
        return null
    }
}