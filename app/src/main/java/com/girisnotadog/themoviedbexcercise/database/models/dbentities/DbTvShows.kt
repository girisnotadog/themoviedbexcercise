package com.girisnotadog.themoviedbexcercise.database.models.dbentities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.girisnotadog.themoviedbexcercise.database.models.domain.TvShowEntity

@Entity(tableName = "tv_shows")
data class DbTvShows(
    @PrimaryKey
    val id : Int
)

fun DbTvShows.toEntityModel() : TvShowEntity {
    return TvShowEntity(
        algo = 0
    ).apply {
        id = id
    }
}