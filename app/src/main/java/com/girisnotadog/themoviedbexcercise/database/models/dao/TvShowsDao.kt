package com.girisnotadog.themoviedbexcercise.database.models.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.girisnotadog.themoviedbexcercise.api.dto.TvShowResultDto
import com.girisnotadog.themoviedbexcercise.api.dto.toDbModel
import com.girisnotadog.themoviedbexcercise.database.models.dbentities.DbMovie
import com.girisnotadog.themoviedbexcercise.database.models.dbentities.DbTvShows

@Dao
interface TvShowsDao : BaseDao<DbMovie> {
    @Query("SELECT * FROM tv_shows")
    fun getAll() : LiveData<List<DbTvShows>>

    fun insertDto(tvShowsListDto : ArrayList<TvShowResultDto>) {
        tvShowsListDto.forEach {
            insert(it.toDbModel())
        }
    }
}


