package com.girisnotadog.themoviedbexcercise.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.girisnotadog.themoviedbexcercise.database.models.dao.MoviesDao
import com.girisnotadog.themoviedbexcercise.database.models.dao.TvShowsDao
import com.girisnotadog.themoviedbexcercise.database.models.dbentities.DbMovie
import com.girisnotadog.themoviedbexcercise.database.models.dbentities.DbTvShows

@TypeConverters(Converters::class)
@Database(
    entities = [
        DbMovie::class,
        DbTvShows::class
    ],
    version = 2
)
abstract class AppDatabase : RoomDatabase() {
    // Abstract functions
    abstract fun moviesDao() : MoviesDao
    abstract fun tvShowsDao() : TvShowsDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        @Synchronized
        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null)
                INSTANCE = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    "movies_db"
                )
                    .fallbackToDestructiveMigration()
                    //.allowMainThreadQueries()
                    .build()

            return INSTANCE!!
        }
    }
}