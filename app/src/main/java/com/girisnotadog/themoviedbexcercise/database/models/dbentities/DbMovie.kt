package com.girisnotadog.themoviedbexcercise.database.models.dbentities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.girisnotadog.themoviedbexcercise.database.models.domain.MediaType
import com.girisnotadog.themoviedbexcercise.database.models.domain.MovieEntity

@Entity(tableName = "movies")
data class DbMovie(
    @PrimaryKey
    val id : Int,
    val title : String,
    val adult : Boolean,
    val image : String,
    val videos : Boolean,
    val favorite : Boolean,
    val playingNow : Boolean,
    val mostRated : Boolean,
    val type : Int,
    val genres : String,
    val releaseDate : String
)

fun DbMovie.toDomainModel() : MovieEntity {
    return MovieEntity(videos, playingNow, mostRated, releaseDate).also {
        it.id = id
        it.title = title
        it.adult = adult
        it.image = image
        it.favorite = favorite
        it.type = MediaType.MOVIE
        it.genres = genres
    }
}
