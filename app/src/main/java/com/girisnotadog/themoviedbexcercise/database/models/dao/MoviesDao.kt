package com.girisnotadog.themoviedbexcercise.database.models.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Update
import com.girisnotadog.themoviedbexcercise.api.dto.MovieResultDto
import com.girisnotadog.themoviedbexcercise.api.dto.toDbModel
import com.girisnotadog.themoviedbexcercise.database.models.dbentities.DbMovie

@Dao
interface MoviesDao : BaseDao<DbMovie> {
    @Query("select * from movies")
    fun getAll() : LiveData<List<DbMovie>>

    @Query("SELECT * FROM movies WHERE playingNow = 1")
    fun getNowPlaying() : LiveData<List<DbMovie>>

    @Query("SELECT * FROM movies WHERE mostRated = 1")
    fun getMostRated() : LiveData<List<DbMovie>>

    fun insertResultDto( movieList : List<MovieResultDto>, type : Int) {
        movieList.forEach {
            insert(it.toDbModel(type))
        }
    }

    fun insertFavorites( movieList: ArrayList<MovieResultDto> ) {
        resetFavorites()

        setFavorites(movieList.map {
            it.id
        })
    }

    @Query("UPDATE movies SET favorite = 1 WHERE id in (:movies)")
    fun setFavorites(movies : List<Int>) : Int

    @Query("UPDATE movies SET favorite = 0")
    fun resetFavorites() : Int
}
