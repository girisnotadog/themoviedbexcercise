package com.girisnotadog.themoviedbexcercise.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.girisnotadog.themoviedbexcercise.R
import com.girisnotadog.themoviedbexcercise.activities.OnSelectItemInterface
import com.girisnotadog.themoviedbexcercise.database.models.domain.MovieEntity
import com.squareup.picasso.Picasso

class MovieListRVAdapter(var mediaList : ArrayList<MovieEntity>, private val callback : OnSelectItemInterface) : RecyclerView.Adapter<MovieListRVAdapter.ViewHolder>() {
    private var context : Context? = null

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val title = itemView.findViewById<TextView>(R.id.media_list_item_title)
        private val genres = itemView.findViewById<TextView>(R.id.media_list_item_genres)
        private val image = itemView.findViewById<ImageView>(R.id.media_list_item_image)
        private val favorite = itemView.findViewById<ImageView>(R.id.media_list_item_favorite_icon)
        private val adult = itemView.findViewById<TextView>(R.id.media_list_item_adult)
        private val release_date = itemView.findViewById<TextView>(R.id.media_list_item_release_date)
        private val layout = itemView.findViewById<FrameLayout>(R.id.media_list_item_layout)

        fun bind(media : MovieEntity) {
            title.text = media.title
            genres.text = media.genres
            release_date.text = media.releaseDate

            layout.setOnClickListener {
                callback.onSelect(media)
            }

            favorite.setOnClickListener {
                callback.onClickFavorite(media)
            }

            context?.let {
                favorite.setImageDrawable(
                    ContextCompat.getDrawable(it,
                        if(media.favorite)
                            R.drawable.ic_baseline_favorite_24
                        else
                            R.drawable.ic_baseline_favorite_border_24
                    )
                )

                Picasso.get()
                    .load(
                        "${it.getString(R.string.imbd_image_url)}${media.image}"
                    )
                    .into(image)
            }

            adult.visibility = if(media.adult) View.VISIBLE else View.GONE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context

        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.media_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mediaList.get(position))
    }

    override fun getItemCount(): Int = mediaList.size
}