package com.girisnotadog.themoviedbexcercise.adapters

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.girisnotadog.themoviedbexcercise.R
import com.girisnotadog.themoviedbexcercise.api.dto.VideoDto

class VideoListRVAdapter(private val videoList : ArrayList<VideoDto>) : RecyclerView.Adapter<VideoListRVAdapter.ViewHolder>() {
    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val name = itemView.findViewById<TextView>(R.id.video_list_item_name)
        private val official = itemView.findViewById<TextView>(R.id.video_list_item_official)
        private val site = itemView.findViewById<TextView>(R.id.video_list_item_site)
        private val layout = itemView.findViewById<ConstraintLayout>(R.id.video_list_item_layout)

        fun bind(video : VideoDto) {
            name.text = video.name
            official.text = if(video.official) "Oficial" else "No oficial"
            site.text = video.site
            layout.setOnClickListener {
                itemView.context.let {
                    it.startActivity(
                        Intent(Intent.ACTION_VIEW).apply {
                            setData(
                                Uri.parse(
                                    when(video.site) {
                                        "YouTube" -> "https://www.youtube.com/watch?v=${video.key}"
                                        else -> "${it.getString(R.string.imbd_video_url)}${video.key}"
                                    }

                                )
                            )
                        }
                    )
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from (
                parent.context
            ).inflate(R.layout.video_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(videoList.get(position))
    }

    override fun getItemCount(): Int = videoList.size
}