package com.girisnotadog.themoviedbexcercise.api.dto

data class NewSessionDto(
    val success : Boolean,
    val session_id : String
)
