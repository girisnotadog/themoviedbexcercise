package com.girisnotadog.themoviedbexcercise.api.dto

data class NowPlayingDto (
    val page : Int?,
    val results : List<MovieResultDto>,
    val dates : DatesDto,
    val total_pages : Int,
    val total_results : Int
)
