package com.girisnotadog.themoviedbexcercise.api.interfaces

import com.girisnotadog.themoviedbexcercise.api.dto.*
import com.girisnotadog.themoviedbexcercise.api.dto.output.MarkAsFavoriteDto
import retrofit2.http.*

interface ApiMoviesInterface {
    @GET("/3/movie/now_playing")
    suspend fun nowPlaying(
        @Query("language")
        language : String? = "en-US",

        @Query("page")
        page : Int = 1,

        @Query("region")
        region : String? = null
    ) : NowPlayingDto

    @GET("/3/movie/popular")
    suspend fun popularMovies(
        @Query("language")
        language : String? = "en-US",

        @Query("page")
        page : Int = 1,

        @Query("region")
        region : String? = null
    ) : ApiResponse<PopularMovies>

    @GET("/3/movie/{movie_id}")
    suspend fun movieDetails(
        @Path("movie_id")
        movie_id : Int,

        @Query("language")
        language : String? = "en-US",

        @Query("page")
        page : Int = 1,

        @Query("region")
        region : String? = null
    ) : ApiResponse<MovieDetailDto>

    @GET("/3/movie/{movie_id}/videos")
    suspend fun movieVideos(
        @Path("movie_id")
        movie_id: Int,

        @Query("language")
        language : String? = "en-US",
    ) : VideoListDto

    @GET("/3/account/{account_id}/favorite/movies")
    suspend fun markFavoriteMovie(
        @Query("account_id")
        account_id : Int,
    ) : ApiResponse<ArrayList<MovieResultDto>>

    @GET("/3/account/{account_id}/favorite/movies")
    suspend fun favoritesMovies(
        @Query("account_id")
        account_id : Int,
    ) : ApiResponse<FavoritesMoviesDto>
}