package com.girisnotadog.themoviedbexcercise.api.dto.output

data class CreateSessionDto(
    val request_token : String
)
