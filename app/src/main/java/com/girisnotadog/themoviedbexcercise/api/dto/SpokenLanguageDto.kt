package com.girisnotadog.themoviedbexcercise.api.dto

data class SpokenLanguageDto(
    val iso_639_1 : String,
    val name : String
)
