package com.girisnotadog.themoviedbexcercise.api

import android.util.Log
import com.girisnotadog.themoviedbexcercise.TheMovieDbApp
import com.girisnotadog.themoviedbexcercise.api.interfaces.ApiMoviesInterface
import com.girisnotadog.themoviedbexcercise.api.interfaces.ApiTvShowsInterface
import com.girisnotadog.themoviedbexcercise.api.interfaces.ApiUserInterface
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object APIClient {
    val BASE_URL = "https://api.themoviedb.org/"

    // 15 segundos para que falle la petición
    private val readTimeout : Long = 15
    private val lstorage = TheMovieDbApp.getInstance().localStorage

    private val client = OkHttpClient
        .Builder()
        .readTimeout(readTimeout, TimeUnit.SECONDS)
        .addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {

                Log.d("API_KEY", lstorage.getApiKey())
                val request = chain.request()

                val url = request
                    .url
                    .newBuilder()
                    .addQueryParameter(
                        "api_key",
                        lstorage.getApiKey()
                    )
                    .build()

                Log.d("PeticionURL", url.toString())

                return chain.proceed(
                    request
                        .newBuilder()
                        .addHeader("Accept", "application/json")
                        .url(
                            url
                        )
                        .build()
                )
            }
        })
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    private fun <T> buildService( service : Class<T> ) : T = retrofit.create(service)

    fun movies() : ApiMoviesInterface = buildService(ApiMoviesInterface::class.java)
    fun user() : ApiUserInterface = buildService(ApiUserInterface::class.java)
    fun tvShows() : ApiTvShowsInterface = buildService(ApiTvShowsInterface::class.java)
}