package com.girisnotadog.themoviedbexcercise.api.dto

data class VideoListDto(
    val id : Int,
    val results : ArrayList<VideoDto>
)
