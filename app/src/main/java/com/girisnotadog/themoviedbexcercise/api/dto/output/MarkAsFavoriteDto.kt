package com.girisnotadog.themoviedbexcercise.api.dto.output

data class MarkAsFavoriteDto(
    var media_type : String, // Allowed values: movie, tv
    var media_id : Int,
    var favorite : Boolean
)
