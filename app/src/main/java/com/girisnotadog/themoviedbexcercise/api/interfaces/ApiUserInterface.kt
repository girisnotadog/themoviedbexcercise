package com.girisnotadog.themoviedbexcercise.api.interfaces

import com.girisnotadog.themoviedbexcercise.api.dto.*
import com.girisnotadog.themoviedbexcercise.api.dto.output.CreateSessionDto
import com.girisnotadog.themoviedbexcercise.api.dto.output.MarkAsFavoriteDto
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiUserInterface {
    @GET("/3/account")
    suspend fun account(
        @Query("session_id")
        session_id : String = ""
    ) : AccountDetailsDto

    @POST("/3/account/{account_id}/favorite")
    suspend fun markAsFavorite(
        @Query("account_id")
        account_id : Int,

        @Body()
        favoriteItem : MarkAsFavoriteDto
    ) : ApiResponse<Any?>

    @POST("/3/authentication/session/new")
    suspend fun createSession(
        @Body
        body : CreateSessionDto
    ) : NewSessionDto

    @GET("/3/authentication/token/new")
    suspend fun createRequestToken() : RequestTokenDto

    @GET("/3/authentication/guest_session/new")
    suspend fun createGuestSession() : NewGuestSessionDto
}