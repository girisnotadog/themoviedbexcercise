package com.girisnotadog.themoviedbexcercise.api.dto

data class GenreDto(
    val id : Int,
    val name : String
)
