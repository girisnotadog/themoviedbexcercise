package com.girisnotadog.themoviedbexcercise.api.dto

open class BaseApiResponse (
    open var status_code : Int? = 1,
    open var status_message : String? = null,
    open var success : Boolean = true
)

data class ApiResponse<T> (
    val data : T
) : BaseApiResponse()