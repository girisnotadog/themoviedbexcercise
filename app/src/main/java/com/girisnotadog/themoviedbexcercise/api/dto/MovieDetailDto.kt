package com.girisnotadog.themoviedbexcercise.api.dto

data class MovieDetailDto(
    val id : Int,
    val adult : Boolean,
    val backdrop_path : String?,
    //val belongs_to_collection : null or object,
    val budget : Int,
    val genres : ArrayList<GenreDto>,
    val homepage : String?,
    val imdb_id : String?, // minLength : 9, maxLength : 9 pattern : ^tt[0-9]{7}
    val original_language : String,
    val original_title : String,
    val overview : String?,
    val popularity : Int,
    val poster_path : String?,
    val production_companies : ArrayList<ProductionCompanyDto>,
    val production_countries : ArrayList<ProductionCountryDto>,
    val release_date : String, // format : date
    val revenue : Int,
    val runtime : Int?,
    val spoken_languages : ArrayList<SpokenLanguageDto>,
    val iso_639_1 : String,
    val name : String,
    val status : String, // Allowed  Values: Rumored, Planned, In Production, Post Production, Released, Canceled,
    val tagline : String?,
    val title : String,
    val video : Boolean,
    val vote_average : Int,
    val vote_count : Int,
)
