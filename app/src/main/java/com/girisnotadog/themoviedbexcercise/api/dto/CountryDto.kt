package com.girisnotadog.themoviedbexcercise.api.dto

data class CountryDto(
    val iso_3166_1 : String,
    val english_name : String
)
