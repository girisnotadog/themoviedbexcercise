package com.girisnotadog.themoviedbexcercise.api.dto

data class ProductionCompanyDto(
    val id : Int,
    val name : String,
    val logo_path : String?,
    val origin_country : String,
)
