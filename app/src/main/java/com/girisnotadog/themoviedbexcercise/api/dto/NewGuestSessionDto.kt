package com.girisnotadog.themoviedbexcercise.api.dto

data class NewGuestSessionDto(
    val success : Boolean,
    val guest_session_id : String,
    val expires_at : String
)
