package com.girisnotadog.themoviedbexcercise.api.interfaces

import com.girisnotadog.themoviedbexcercise.api.dto.ApiResponse
import com.girisnotadog.themoviedbexcercise.api.dto.FavoritesTvShowsDto
import com.girisnotadog.themoviedbexcercise.api.dto.VideoListDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiTvShowsInterface {
    @GET("/3/tv/{tv_id}/videos")
    suspend fun tvVideos(
        @Path("tv_id")
        tv_id: Int,

        @Query("language")
        language : String? = "en-US",
    ) : ApiResponse<VideoListDto>

    @GET("/3/account/{account_id}/favorite/tv")
    suspend fun getFavoriteTvShows(
        @Query("account_id")
        account_id : Int,
    ) : ApiResponse<FavoritesTvShowsDto>
}