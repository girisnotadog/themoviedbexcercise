package com.girisnotadog.themoviedbexcercise.api.dto

data class RequestTokenDto(
    val success : Boolean,
    val expires_at : String,
    val request_token : String
)
