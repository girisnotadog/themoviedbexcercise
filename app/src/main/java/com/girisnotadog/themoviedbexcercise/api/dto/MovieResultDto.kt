package com.girisnotadog.themoviedbexcercise.api.dto

import com.girisnotadog.themoviedbexcercise.database.models.dbentities.DbMovie

data class MovieResultDto(
    val id : Int,
    val adult : Boolean,
    val backdrop_path : String?,
    val genre_ids : List<Int>,
    val original_title : String,
    val original_language : String,
    val overview : String,
    val popularity : Float,
    val poster_path : String?,
    val release_date : String,
    val title : String,
    val video : Boolean,
    val vote_average : Float,
    val vote_count : Int
)

fun MovieResultDto.toDbModel(type : Int) : DbMovie {
    return DbMovie(
        id = id,
        favorite = false,
        mostRated = type == 1,
        playingNow = type == 0,
        adult = adult,
        image = "${poster_path}",
        title = title,
        type = type,
        videos = video,
        genres = "",
        releaseDate = release_date
    )
}
