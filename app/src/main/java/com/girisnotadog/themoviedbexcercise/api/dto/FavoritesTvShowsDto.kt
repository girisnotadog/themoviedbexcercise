package com.girisnotadog.themoviedbexcercise.api.dto

data class FavoritesTvShowsDto(
    val page : Int,
    val results : ArrayList<TvShowResultDto>,
    val total_pages : Int,
    val total_results : Int
)
