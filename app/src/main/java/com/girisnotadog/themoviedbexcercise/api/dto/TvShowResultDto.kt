package com.girisnotadog.themoviedbexcercise.api.dto

import com.girisnotadog.themoviedbexcercise.database.models.dbentities.DbMovie

data class TvShowResultDto(
    val id : Int,
    val popularity : Int,
    val backdrop_path : String?,
    val poster_path : String,
    val vote_average : Int,
    val overview : String,
    val first_air_date : String,
    val origin_country : ArrayList<String>,
    val genre_ids : ArrayList<Int>,
    val original_language : String,
    val vote_count : Int,
    val name : String,
    val original_name : String,
    val releaseDate : String
)

fun TvShowResultDto.toDbModel(): DbMovie {
    return DbMovie(
        id = id,
        title = name,
        adult = false,
        image = poster_path,
        videos = false,
        favorite = false,
        playingNow = false,
        mostRated = false,
        type = 3,
        genres = "",
        releaseDate = releaseDate
    )
}