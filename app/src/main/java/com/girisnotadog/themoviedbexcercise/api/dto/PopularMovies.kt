package com.girisnotadog.themoviedbexcercise.api.dto

data class PopularMovies (
    val page : Int,
    val results : ArrayList<MovieResultDto>,
    val total_results : Int,
    val total_pages : Int
)
