package com.girisnotadog.themoviedbexcercise.api.dto

data class DatesDto (
    val maximum : String,
    val minimum : String
)
