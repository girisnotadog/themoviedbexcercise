package com.girisnotadog.themoviedbexcercise.api.dto

data class FavoritesMoviesDto(
    val page : Int,
    val results : ArrayList<MovieResultDto>
)
