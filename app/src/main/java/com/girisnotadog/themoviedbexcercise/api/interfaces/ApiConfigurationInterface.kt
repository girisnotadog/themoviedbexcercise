package com.girisnotadog.themoviedbexcercise.api.interfaces

import com.girisnotadog.themoviedbexcercise.api.dto.ApiResponse
import com.girisnotadog.themoviedbexcercise.api.dto.CountryDto
import com.girisnotadog.themoviedbexcercise.api.dto.LanguageDto
import retrofit2.http.GET

interface ApiConfigurationInterface {
    @GET("/3/configuration/countries")
    suspend fun countries () : ApiResponse<ArrayList<CountryDto>>

    @GET("/3/configuration/languages")
    suspend fun languages() : ApiResponse<ArrayList<LanguageDto>>
}