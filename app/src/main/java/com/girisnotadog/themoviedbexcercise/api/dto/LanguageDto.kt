package com.girisnotadog.themoviedbexcercise.api.dto

data class LanguageDto(
    val iso_639_1 : String,
    val english_name : String,
    val name : String
)
