package com.girisnotadog.themoviedbexcercise.api.dto

data class VideoDto(
    val id : String,
    val name : String,
    val type : String,
    val official : Boolean,
    val size : Int,
    val key : String,
    val site : String,
    val iso_639_1 : String,
    val iso_3166_1 : String,
)
