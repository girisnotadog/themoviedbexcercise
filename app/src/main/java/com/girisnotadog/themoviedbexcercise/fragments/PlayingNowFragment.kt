package com.girisnotadog.themoviedbexcercise.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.girisnotadog.themoviedbexcercise.activities.OnSelectItemInterface
import com.girisnotadog.themoviedbexcercise.adapters.MovieListRVAdapter
import com.girisnotadog.themoviedbexcercise.api.Resource
import com.girisnotadog.themoviedbexcercise.databinding.FragmentPlayingNowBinding
import com.girisnotadog.themoviedbexcercise.viewmodels.MediaListViewModel

class PlayingNowFragment : Fragment() {
    private lateinit var binding : FragmentPlayingNowBinding
    private val viewModel : MediaListViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPlayingNowBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setup()
    }

    fun setup() {
        binding.fragmentPlayingNowRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        viewModel.getNowPlayingMovies().observe(viewLifecycleOwner) {
            binding.fragmentPlayingNowLoading.visibility = View.GONE
            when(it.status) {
                Resource.Status.SUCCESS -> {

                    binding.fragmentPlayingNowRecyclerView.adapter = MovieListRVAdapter(
                        it.data !!,
                        activity as OnSelectItemInterface
                    )
                }
                Resource.Status.ERROR -> Toast.makeText(context, "Error al cargar reproducción actual: ${it.message}", Toast.LENGTH_SHORT).show()
                Resource.Status.LOADING -> binding.fragmentPlayingNowLoading.visibility = View.VISIBLE
            }

        }

    }

    companion object {
        @JvmStatic
        fun newInstance() = PlayingNowFragment()
    }
}

