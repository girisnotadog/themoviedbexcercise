package com.girisnotadog.themoviedbexcercise.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import com.girisnotadog.themoviedbexcercise.R
import com.girisnotadog.themoviedbexcercise.api.Resource
import com.girisnotadog.themoviedbexcercise.databinding.FragmentSettingsBinding
import com.girisnotadog.themoviedbexcercise.viewmodels.SettingsViewModel

class SettingsFragment : Fragment() {
    private lateinit var binding : FragmentSettingsBinding
    private val viewModel : SettingsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSettingsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.api_key.observe(viewLifecycleOwner) {
            binding.fragmentSettingsApiKey.editText?.setText( it )
        }

        viewModel.user_session_id.observe(viewLifecycleOwner) {
            binding.fragmentSettingsSessionId.editText?.setText( it )
        }

        viewModel.account_id.observe(viewLifecycleOwner) {
            binding.fragmentSettingsAccountId.editText?.setText( "${it}" )
        }

        binding.fragmentSettingsSaveButton.setOnClickListener {
            saveInputs()
            viewModel.saveSettings()

            Toast.makeText(context, getString(R.string.settings_saved), Toast.LENGTH_SHORT).show()
        }

        binding.fragmentSettingsRefreshSessionButton.setOnClickListener {
            viewModel.createRequestToken().observe(viewLifecycleOwner) {
                when(it.status) {
                    Resource.Status.SUCCESS -> {
                        if(it.data == true) {
                            refreshSessionId()
                        }
                    }
                    Resource.Status.ERROR -> Log.d("RequestToken", "ERROR: ${it.message}")
                    Resource.Status.LOADING -> Log.d("RequestToken", "LOADING")
                }
            }
        }
    }

    private fun refreshSessionId() {
        viewModel.refreshSessionId().observe(viewLifecycleOwner) {
            when(it.status) {
                Resource.Status.SUCCESS -> {
                    viewModel.user_session_id.value = viewModel.getUserSessionIdLS()
                    getAccountId()
                }
                Resource.Status.ERROR -> {Log.d("SessionID", "ERROR: ${it.message}")}
                Resource.Status.LOADING -> {Log.d("SessionID", "LOADING")}
            }
        }
    }

    private fun getAccountId() {
        viewModel.getAccountId().observe(viewLifecycleOwner) {
            when(it.status) {
                Resource.Status.SUCCESS -> Log.d("SessionID", "ADQUIRED")
                Resource.Status.ERROR -> Log.d("SessionID", "ERROR: ${it.message}")
                Resource.Status.LOADING -> Log.d("SessionID", "LOADING")
            }
        }
    }

    private fun saveInputs() {
        viewModel.api_key.value = binding.fragmentSettingsApiKeyEdit.editableText.toString()

        viewModel.user_session_id.value = binding.fragmentSettingsSessionIdEdit.editableText.toString()
    }

    override fun onPause() {
        super.onPause()
        saveInputs()
    }

    companion object {
        @JvmStatic
        fun newInstance() = SettingsFragment()
    }
}