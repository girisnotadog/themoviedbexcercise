package com.girisnotadog.themoviedbexcercise.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.girisnotadog.themoviedbexcercise.R
import com.girisnotadog.themoviedbexcercise.activities.OnSelectItemInterface
import com.girisnotadog.themoviedbexcercise.databinding.FragmentMovieDetailsBinding
import com.girisnotadog.themoviedbexcercise.viewmodels.MediaViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.squareup.picasso.Picasso

class MovieDetailsFragment : BottomSheetDialogFragment() {
    private lateinit var binding : FragmentMovieDetailsBinding
    private val viewModel : MediaViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMovieDetailsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.media.value?.let {
            binding.fragmentMovieDetailsTitle.text = it.title

            Picasso.get().load(
                "${getString(R.string.imbd_image_url)}${it.image}"
            ).into(
                binding.fragmentMovieDetailsImage
            )

            binding.fragmentMovieDetailsMoviesLink.setOnClickListener {
                (activity as OnSelectItemInterface).onWatchVideos()
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = MovieDetailsFragment()
    }
}