package com.girisnotadog.themoviedbexcercise.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.girisnotadog.themoviedbexcercise.adapters.VideoListRVAdapter
import com.girisnotadog.themoviedbexcercise.api.Resource
import com.girisnotadog.themoviedbexcercise.database.models.domain.MediaItem
import com.girisnotadog.themoviedbexcercise.databinding.FragmentMoviePlayerBinding
import com.girisnotadog.themoviedbexcercise.viewmodels.MediaViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class MoviePlayerFragment : BottomSheetDialogFragment() {
    private lateinit var binding : FragmentMoviePlayerBinding
    private val viewModel : MediaViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMoviePlayerBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.fragmentMoviePlayerVideoList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        viewModel.media.observe(viewLifecycleOwner) {
            loadVideos(it)
        }
    }

    fun loadVideos(mediaItem: MediaItem) {
        viewModel.mediaVideos(mediaItem).observe(viewLifecycleOwner) {
            when(it.status) {
                Resource.Status.SUCCESS -> {
                    it.data?.let {
                        binding.fragmentMoviePlayerVideoList.adapter = VideoListRVAdapter(it)
                    }
                }
                Resource.Status.ERROR -> Toast.makeText(context, "Error al cargar videos: ${it.message}", Toast.LENGTH_SHORT).show()
                Resource.Status.LOADING -> {}
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = MoviePlayerFragment()
    }
}