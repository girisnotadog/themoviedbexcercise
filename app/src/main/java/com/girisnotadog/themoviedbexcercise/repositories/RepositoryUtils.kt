package com.girisnotadog.themoviedbexcercise.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.girisnotadog.themoviedbexcercise.api.Resource
import kotlinx.coroutines.Dispatchers

open class RepositoryUtils {
    fun <T, A> performSimpleGetOperation(
        networkCall: suspend () -> A,
        transformResponse : suspend (A) -> T
    ) : LiveData<Resource<T>> = liveData (Dispatchers.IO) {
        emit(Resource.loading())

        try {
            val callResult = networkCall.invoke()
            emit( Resource.success(transformResponse(callResult)) )
        }catch (e : Exception) {
            Log.e("performSimpleGetOperation", "${e.message}")
            emit(Resource.error("${e.message}", null))
        }
    }

    fun <T, A> performGetOperation(databaseQuery: () -> LiveData<T>,
               networkCall: suspend () -> A,
               saveCallResult: suspend (A) -> Unit): LiveData<Resource<T>> =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val source = databaseQuery.invoke().map { Resource.success(it) }
            emitSource(source)

            try {
                val responseStatus = networkCall.invoke()
                saveCallResult(responseStatus)
            }catch (e : Exception) {
                Log.e("performGetOperation", e.stackTraceToString())
                emit(Resource.error("Servicio no disponible, intente más tarde", null))
            }finally {
                emitSource(source)
            }
        }
}