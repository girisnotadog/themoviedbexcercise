package com.girisnotadog.themoviedbexcercise.repositories

import android.util.Log
import androidx.lifecycle.liveData
import com.girisnotadog.themoviedbexcercise.TheMovieDbApp
import com.girisnotadog.themoviedbexcercise.api.APIClient
import com.girisnotadog.themoviedbexcercise.api.dto.AccountDetailsDto
import com.girisnotadog.themoviedbexcercise.api.dto.NewSessionDto
import com.girisnotadog.themoviedbexcercise.api.dto.output.CreateSessionDto

class SettingsRepository : RepositoryUtils() {
    private val lstorage = TheMovieDbApp.getInstance().localStorage

    fun getSessionId() = performGetOperation(
        databaseQuery = {
            liveData {
                emit( lstorage.getUserSessionId() )
            }
        },
        networkCall = {
            if(lstorage.getUserSessionId().isNullOrEmpty()) {
                APIClient.user().createSession(
                    CreateSessionDto(
                        lstorage.getRequestToken()
                    )
                )
            }
            else
                NewSessionDto(success = true, session_id = lstorage.getUserSessionId() !! )
        },
        saveCallResult = {
            lstorage.setUserSessionId( it.session_id )
        }
    )

    fun getGuestSessionId() = performGetOperation(
        databaseQuery = {
            liveData {
                emit ( lstorage.getUserSessionId() )
            }
        },
        networkCall = {
             APIClient.user().createGuestSession()
        },
        saveCallResult = {
            lstorage.setUserSessionId(it.guest_session_id)
        }
    )

    fun createRequestToken() = performSimpleGetOperation(
        networkCall = {
            APIClient.user().createRequestToken()
        },
        transformResponse = {
            Log.d("createRequestToken", it.request_token)
            lstorage.setRequestToken( it.request_token )
        }
    )

    fun getAccountId() = performSimpleGetOperation(
        networkCall = {
            APIClient.user().account( "${lstorage.getUserSessionId()}" )
        },
        transformResponse = {
            lstorage.setUserAccountId( it.id )
        }
    )
}