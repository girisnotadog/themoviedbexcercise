package com.girisnotadog.themoviedbexcercise.repositories

import android.util.Log
import androidx.lifecycle.Transformations
import androidx.lifecycle.distinctUntilChanged
import com.girisnotadog.themoviedbexcercise.TheMovieDbApp
import com.girisnotadog.themoviedbexcercise.api.APIClient
import com.girisnotadog.themoviedbexcercise.api.dto.output.MarkAsFavoriteDto
import com.girisnotadog.themoviedbexcercise.database.AppDatabase
import com.girisnotadog.themoviedbexcercise.database.models.dbentities.toDomainModel

class MoviesRepository (
    database: AppDatabase
) : RepositoryUtils() {
    private val daoMovies = database.moviesDao()
    private val lstorage = TheMovieDbApp.getInstance().localStorage

    fun getMostPopularMovies() = performGetOperation(
        databaseQuery = {
            Transformations.map( daoMovies.getAll().distinctUntilChanged() ) {
                it.mapTo(ArrayList()) {
                    it.toDomainModel()
                }
            }
        },
        networkCall = {
            APIClient.movies().popularMovies()
        },
        saveCallResult = {
            daoMovies.insertResultDto( it.data.results, 2 )
        }
    )

    fun getNowPlayingMovies() = performGetOperation(
        databaseQuery = {
            Transformations.map( daoMovies.getAll().distinctUntilChanged() ) {
            //Transformations.map( daoMovies.getNowPlaying().distinctUntilChanged() ) {
                it.mapTo(ArrayList()) {
                    it.toDomainModel()
                }
            }
        },
        networkCall = {
            APIClient.movies().nowPlaying()
        },
        saveCallResult = {
            Log.d("callResults", it.toString())
            daoMovies.insertResultDto(it.results, 1)
        }
    )

    fun setFavorite(movie_id : Int, favorite : Boolean) = performSimpleGetOperation(
        {
            val session_id : String? = lstorage.getUserSessionId()

            if(session_id.isNullOrEmpty()) {
                session_id?.let {
                    APIClient.user().markAsFavorite(
                        it.toInt(),
                        MarkAsFavoriteDto("movie", movie_id, favorite) // TODO: Check if "movie" type is a valid type
                    )
                }
            }else{
                Log.e("setFavorite", "Id de sessión inválido")
            }
        },{
            it // Return all the object
        }
    )

    fun refreshFavorites() = performSimpleGetOperation(
        {
            val account_id : Int = lstorage.getAccountId()

            APIClient.movies().favoritesMovies(
                account_id
            )
        },
        {
            daoMovies.insertFavorites(it.data.results)
        }
    )

    fun getMediaVideos(movie_id: Int) = performSimpleGetOperation(
        networkCall = {
            APIClient.movies().movieVideos(movie_id)
        },
        transformResponse = {
            it.results
        }
    )
}