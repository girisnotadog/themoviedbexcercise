package com.girisnotadog.themoviedbexcercise.repositories

import androidx.lifecycle.Transformations
import androidx.lifecycle.distinctUntilChanged
import com.girisnotadog.themoviedbexcercise.TheMovieDbApp
import com.girisnotadog.themoviedbexcercise.api.APIClient
import com.girisnotadog.themoviedbexcercise.database.AppDatabase
import com.girisnotadog.themoviedbexcercise.database.models.dbentities.toEntityModel

class TvShowsRepository(
    database: AppDatabase
) : RepositoryUtils() {
    private val DaoTvShows = database.tvShowsDao()
    private val lstorage = TheMovieDbApp.getInstance().localStorage

    fun getMostRatedTvShowsList() = performGetOperation(
        databaseQuery = {
            Transformations.map( DaoTvShows.getAll().distinctUntilChanged() ) {
                it.mapTo(ArrayList()) {
                    it.toEntityModel()
                }
            }
        },
        networkCall = {
            APIClient.tvShows().getFavoriteTvShows(
                account_id = lstorage.getAccountId()
            )
        },
        saveCallResult = {
            DaoTvShows.insertDto(it.data.results)
        }
    )
}