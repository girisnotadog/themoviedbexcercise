package com.girisnotadog.themoviedbexcercise.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.girisnotadog.themoviedbexcercise.R
import com.girisnotadog.themoviedbexcercise.api.Resource
import com.girisnotadog.themoviedbexcercise.database.models.domain.MediaItem
import com.girisnotadog.themoviedbexcercise.databinding.ActivityMainBinding
import com.girisnotadog.themoviedbexcercise.fragments.*
import com.girisnotadog.themoviedbexcercise.viewmodels.MediaViewModel

class MainActivity : AppCompatActivity(), OnSelectItemInterface {
    private lateinit var binding : ActivityMainBinding
    private lateinit var viewModel : MediaViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MediaViewModel::class.java)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        setup()
    }

    private fun setup() {
        viewModel.tab.observe(this) {
            when(it) {
                1 -> showFragment(MostPopularFragment.newInstance())
                2 -> showFragment(PlayingNowFragment.newInstance())
                3 -> showFragment(SettingsFragment.newInstance())
            }
        }

        binding.activityMainBottomNavigation.setOnItemSelectedListener {
            when(it.itemId) {
                R.id.menu_item_most_popular -> viewModel.tab.value = 1
                R.id.menu_item_playing_now -> viewModel.tab.value = 2
                R.id.menu_item_settings -> viewModel.tab.value = 3
            }
            true
        }
    }

    private fun showFragment(fragment : Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(binding.mainActivityContentView.id, fragment)
            .addToBackStack(null)
            .commit()
    }

    override fun onSelect(media: MediaItem) {
        viewModel.media.value = media

        MovieDetailsFragment.newInstance().show(supportFragmentManager, "movieDetails")
    }

    override fun onWatchVideos() {
        MoviePlayerFragment.newInstance().show(supportFragmentManager, "movieVideoList")
    }

    override fun onClickFavorite(media: MediaItem) {
        viewModel.setFavorite(media).observe(this) {
            binding.activityMainLoader.visibility = View.GONE
            when(it.status) {
                Resource.Status.SUCCESS -> {
                    refreshFavorites()
                }
                Resource.Status.ERROR -> Toast.makeText(applicationContext, "Error al definir favorito: ${it.message}", Toast.LENGTH_SHORT).show()
                Resource.Status.LOADING -> binding.activityMainLoader.visibility = View.VISIBLE
            }
        }
    }

    private fun refreshFavorites() {
        viewModel.refreshFavoriteList().observe(this) {
            binding.activityMainLoader.visibility = View.GONE

            when(it.status) {
                Resource.Status.SUCCESS -> {}
                Resource.Status.ERROR -> Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()
                Resource.Status.LOADING -> binding.activityMainLoader.visibility = View.VISIBLE
            }
        }
    }
}

interface OnSelectItemInterface {
    fun onSelect(media : MediaItem)
    fun onClickFavorite(media : MediaItem)
    fun onWatchVideos()
}