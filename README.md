This is a excercise application used to show the MVVM and the Repository design pattern.

It consumes an API from https://api.themoviedb.org/

To make it function you have to put the api_key in the settings Menu
you can get it from yout account at https://www.themoviedb.org/settings/api

and after it you have to press the button "Obtener ID de sesión" and "Guardar cambios".

with that the application will save the configuration in local storage, so you'll be able to consume the API's.

Some accounts cant access to certains functions, like get a sessionID, so i put a method to use a GuestSessionID in the SettingsViewModel.tk file, just uncomment the desired function to use.

TODO:

Add unit testing
Add local video player
Add option for use guestLogin instead of userLogin when adquiring session_id
Add a login view
Add snackbar for notifications
Add icon to indicate offline mode